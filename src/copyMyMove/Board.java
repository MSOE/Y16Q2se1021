package copyMyMove;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Board extends JPanel {
    private Tile[][] tiles;
    private Player player;
    private Player copyCat;
    private Point finishTile = new Point(0, 0);

    public Board(String filename) throws IOException {
        parseFile(filename);
        setLayout(new GridLayout(getRows(), getCols()));
        for(int r=0; r<getRows(); ++r) {
            for(int c=0; c<getCols(); ++c) {
                add(tiles[r][c]);
            }
        }
    }

    public int getCols() {
        return tiles[0].length;
    }

    public int getRows() {
        return tiles.length;
    }

    private void parseFile(String filename) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(filename));
        int rows = lines.size();
        int columns = lines.get(0).length();
        tiles = new Tile[rows][columns];
        final boolean IS_COPY_CAT = true;
        for(int r = 0; r< tiles.length; ++r) {
            String line = lines.get(r);
            for(int c=0; c<line.length(); ++c) {
                char type = line.charAt(c);
                switch (type) {
                    case '+':
                        player = new Player(c, r);
                        copyCat = new Player(c, r, IS_COPY_CAT);
                        tiles[r][c] = new Tile(' ');
                        tiles[r][c].setText("" + (char)0x222e);
                        break;
                    case '|':
                        player = new Player(c, r);
                        tiles[r][c] = new Tile(' ');
                        tiles[r][c].visit(player);
                        break;
                    case '-':
                        copyCat = new Player(c, r, IS_COPY_CAT);
                        tiles[r][c] = new Tile(' ');
                        tiles[r][c].visit(copyCat);
                        break;
                    case '@':
                        finishTile = new Point(c, r);
                        tiles[r][c] = new Tile(' ');
                        tiles[r][c].setBackground(Color.GRAY);
                        break;
                    default:
                        tiles[r][c] = new Tile(type);
                }
            }
        }
    }

    public boolean isFinished() {
        return arePlayersOnSameTile() && player.getPosition().equals(finishTile);
    }

    public boolean arePlayersOnSameTile() {
        return player.getPosition().equals(copyCat.getPosition());
    }

    public void movePlayers(int move) {
        final int LEFT = 37;
        final int UP = 38;
        final int RIGHT = 39;
        final int DOWN = 40;
        if(move==LEFT || move==UP || move==RIGHT || move==DOWN) {
            int plyRow = (int)player.getPosition().getY();
            int plyCol = (int)player.getPosition().getX();
            int ccRow = (int)copyCat.getPosition().getY();
            int ccCol = (int)copyCat.getPosition().getX();
            int numberMoved = 0;
            switch (move) {
                case LEFT:
                    numberMoved = movePlayers(plyRow, plyCol - 1, ccRow, ccCol + 1);
                    break;
                case UP:
                    numberMoved = movePlayers(plyRow - 1, plyCol, ccRow + 1, ccCol);
                    break;
                case RIGHT:
                    numberMoved = movePlayers(plyRow, plyCol + 1, ccRow, ccCol - 1);
                    break;
                case DOWN:
                    numberMoved = movePlayers(plyRow + 1, plyCol, ccRow - 1, ccCol);
                    break;
            }
            switch (numberMoved) {
                case 2:
                    tiles[ccRow][ccCol].clearPlayer();
                case 1:
                    tiles[plyRow][plyCol].clearPlayer();
            }
            plyRow = (int)player.getPosition().getY();
            plyCol = (int)player.getPosition().getX();
            tiles[plyRow][plyCol].visit(player);
            ccRow = (int)copyCat.getPosition().getY();
            ccCol = (int)copyCat.getPosition().getX();
            tiles[ccRow][ccCol].visit(copyCat);
            if (arePlayersOnSameTile()) {
                tiles[plyRow][plyCol].setText("" + (char) 0x222e);
            }
            if (isFinished()) {
                JOptionPane.showMessageDialog(this, "YOU WIN");
                System.exit(0);
            }
        }
    }

    private int movePlayers(int plyRow, int plyCol, int ccRow, int ccCol) {
        int numberMoved = 0;
        if(plyRow>=0 && plyRow<getRows() && plyCol>=0 && plyCol<getCols() && tiles[plyRow][plyCol].visit(player)) {
            player.setPosition(plyCol, plyRow);
            ++numberMoved;
            if(ccRow>=0 && ccRow<getRows() && ccCol>=0 && ccCol<getCols() && tiles[ccRow][ccCol].visit(copyCat)) {
                copyCat.setPosition(ccCol, ccRow);
                ++numberMoved;
            }
        }
        return numberMoved;
    }

}
