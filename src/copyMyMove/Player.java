package copyMyMove;

import java.awt.*;

public class Player {
    private final boolean IS_COPY_CAT;
    private Point position;

    public Player(int x, int y) {
        this(x, y, false);
    }

    public Player(int x, int y, boolean isCopyCat) {
        position = new Point(x, y);
        this.IS_COPY_CAT = isCopyCat;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(int x, int y) {
        this.position = new Point(x, y);
    }

    @Override
    public String toString() {
        return "" + (IS_COPY_CAT ? (char)0x2299 : (char)0x222b);
    }
}
