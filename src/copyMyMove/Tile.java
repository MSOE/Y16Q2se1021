package copyMyMove;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;

public class Tile extends JButton {
    private char type;

    public Tile(char type) {
        if(type=='.' || type=='_') {
            type = ' ';
        }
        this.type = type;
        setText("" + this.type);
        setFont(new Font("Times Roman", Font.PLAIN, 40));
        setBackground(Color.LIGHT_GRAY);
        setEnabled(false);
    }

    public boolean visit(Player player) {
        boolean allowVisitor = false;
        if(type==' ') {
            allowVisitor = true;
            setText(player.toString());
        }
        return allowVisitor;
    }

    public void clearPlayer() {
        setText("" + type);
    }
}
