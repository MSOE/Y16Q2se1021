package copyMyMove;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

public class Game extends JFrame {
    private static final String LEVEL_PATH = "levels/level";
    private Board board;
    private final static int BUTTON_SIZE = 80;
    private final static int MENU_HEIGHT = 55;

    public Game(String level) throws IOException {
        super("CopyMyMove");
        if(level==null || level.equals("")) {
            level = "1";
        }
        board = new Board(LEVEL_PATH + level + ".txt");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        add(board);
        setSize(board.getCols()*BUTTON_SIZE, board.getRows()*BUTTON_SIZE+MENU_HEIGHT);
        setVisible(true);
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                // Do nothing
            }

            @Override
            public void keyPressed(KeyEvent e) {
                board.movePlayers(e.getKeyCode());
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // Do nothing
            }
        });
    }

    public static void main(String[] args) {
        try {
            String level = JOptionPane.showInputDialog(null, "Please enter level to play.");
            new Game(level);
        } catch (IOException e) {
            System.err.print("Could not read level file. " + e.getMessage());
        }
    }
}
