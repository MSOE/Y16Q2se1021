package wk6;

public class YourOwnException extends Exception {
    public YourOwnException(String message) {
        super(message);
    }
}
