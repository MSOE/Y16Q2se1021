package wk6;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Path path = Paths.get("numbers.txt");
        try (Scanner in = new Scanner(path)) {
            System.out.println("Enter the number of floating point numbers you will enter after this one: ");
            int numberEntered = in.nextInt();
            double total = 0;
            for (int i = 0; i < numberEntered; ++i) {
                total += in.nextDouble();
            }
            System.out.println("The average of those numbers is: " + total / numberEntered);
        } catch (IOException e) {
            System.out.println("Problems reading or finding the file");
        }

    }

    public static void main2(String[] args) throws Exception {
        int[] nums = {0, 1, 2};
        List<Integer> numbers = new ArrayList<>();
        for(int num : nums) {
            numbers.add(num);
        }
        System.out.println("Enter the entry in the array that you would like");
        Scanner in = new Scanner(System.in);
            try {
                System.out.println(numbers.get(in.nextInt()));
                problematicMethod();
            } catch (InputMismatchException e) {
                System.out.println("You needed to enter an integer instead of some terrible garbage");
            } catch (IndexOutOfBoundsException e) {
                Scanner parser = new Scanner(e.getMessage());
                parser.next();
                // 8,
                String indexAndComma = parser.next();
                int index = Integer.parseInt(indexAndComma.substring(0,indexAndComma.length()-1));
                System.out.println("Enter a number between 0 and " + numbers.size() + " not " + index);
            } finally {
                System.out.println("whatever I want");
            }
    }

    private static void problematicMethod() throws IOException {
        throw new IOException("angry");
    }

    public static void main1(String[] args) {
        int age = -1;
        String input = JOptionPane.showInputDialog(null, "Enter your age");
        while(age<0 && input!=null) {
            try {
                age = Integer.parseInt(input);
                if(age<0) {
                    throw new YourOwnException("number was negative");
                }
            } catch (NumberFormatException e) {
                input = JOptionPane.showInputDialog(null, "Please enter an integer for your age");
            } catch (YourOwnException e) {
                System.out.println(e.getMessage());
                input = JOptionPane.showInputDialog(null, "Please enter a non-negative integer for your age");
            }
        }
        JOptionPane.showMessageDialog(null, "You will be " + (age + 1) + " next year");
    }
}
