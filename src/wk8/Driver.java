package wk8;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

public class Driver {
    public static void main(String[] args) {
        writeObjects(Paths.get("objects.txt"), false);
        readObjects(Paths.get("objects.txt"));

    }

    private static void writeObjects(Path path, boolean append) {
        try (FileOutputStream fos = new FileOutputStream(path.toFile(), append);
             ObjectOutputStream out = new ObjectOutputStream(fos)) {
            out.writeInt(178);
            out.writeObject("I am a string");
            out.writeObject(new Date());
            out.writeObject(new StuffToBeWritten());
        } catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        } catch(IOException e) {
            System.err.println("Problems writing to file");
            System.err.println(e.getMessage());
        }
    }

    private static void readObjects(Path path) {
        try (FileInputStream fis = new FileInputStream(path.toFile());
             ObjectInputStream in = new ObjectInputStream(fis)) {
            int i = in.readInt();
            String phrase = (String)in.readObject();
            Object date = in.readObject();
            StuffToBeWritten stuff = (StuffToBeWritten)in.readObject();
            System.out.println(i);
            System.out.println(phrase);
            System.out.println(date);
            System.out.println(stuff);
        } catch (FileNotFoundException e) {
            System.err.println("Could not fine file " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Problems reading file");
            System.err.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            System.err.println("Unexpected object type read\n" + e.getMessage());
        }
    }

    private static String readText(String s) {
        return null;
    }

    private static void writeText(String filename) {
        try (FileOutputStream fos = new FileOutputStream(new File(filename), true);
             PrintWriter out = new PrintWriter(fos)) {
            out.println("WAKE UP");
            out.println("WAKE UP");
            out.print(2.0);
        } catch (FileNotFoundException e) {
            System.out.println("Directory where you wish to place the file does not exist");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void listDirectory(String name) {
        Path path = Paths.get(name);
        DirectoryStream<Path> directory = null;
        try {
            directory = Files.newDirectoryStream(path);
            for(Path p : directory) {
                System.out.println(p);
            }
        } catch (IOException e) {
            System.err.println("Error connecting to directory structure.");
        } finally {
            try {
                if(directory!=null) {
                    directory.close();
                }
            } catch (IOException e) {
                System.err.println("Error cleaning up directory structure resources.");
            }
        }
    }
    private static void listDirectory2(String name) {
        Path path = Paths.get(name);
        try (DirectoryStream<Path> directory = Files.newDirectoryStream(path)) {
            for(Path p : directory) {
                System.out.println(p);
            }
        } catch (IOException e) {
            System.err.println("Error connecting to directory structure.");
        }
    }

    public static void writeBytes(Path path, boolean append) {
        try (FileOutputStream out = new FileOutputStream(path.toFile(), append)) {
            out.write('a');
            out.write('d');
            out.write(1000000); // F4240 -> 11110100001001000000 -> 01000000
            out.write(-1000000);
            byte[] byteArray = { 5, 20, 32, 'C', 'T' };
            out.write(byteArray);
            System.out.println("Data written");
        } catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        } catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void readBytes(Path path) {
        try (FileInputStream in = new FileInputStream(path.toFile())) {
            int i = in.read();
            int j = in.read();
            int k = in.read();
            int l = in.read();
            int m = in.read();
            byte[] byteArray = new byte[5];
            int count = in.read(byteArray);
            if(5!=count) {
                System.err.println("Couldn't find five bytes in the file.  Only read " + count);
            }
            System.out.println("Data read");
            System.out.println("" + i + " " + j + " " + k + " " + l + " " + m);
        }
        catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void writePrimitives(Path path, boolean append) {
        try (FileOutputStream out = new FileOutputStream(path.toFile(), append);
             DataOutputStream dOut = new DataOutputStream(out)) {
            dOut.writeInt(43);
            dOut.writeBoolean(false);
            dOut.writeDouble(Math.PI);
            System.out.println("Data written");
        }
        catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void readPrimitives(Path path) {
        try (FileInputStream in = new FileInputStream(path.toFile());
             DataInputStream dIn = new DataInputStream(in)) {
            int i = dIn.readInt();
            boolean j = dIn.readBoolean();
            double k = dIn.readDouble();
            System.out.println("Data read");
            System.out.println("" + i + " " + j + " " + k);
        }
        catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

}









