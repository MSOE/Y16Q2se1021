package wk8;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class StuffToBeWritten implements Serializable {
    private String a = "The baby was old";
    public Collection<String> data = new ArrayList<>();

    public StuffToBeWritten() {
        data.add("the");
        data.add("grandma");
        data.add("was");
        data.add("yelling");
    }

    @Override
    public String toString() {
        return a + data;
    }
}
