package wk1;

import java.awt.*;

public class Circle extends Shape {
    private double radius;
    private static final double DEFAULT_RADIUS = 1.0;

    public Circle(double x, double y, Color color, double radius) {
        super(x, y, color);
        this.radius = radius;
        System.out.println("Circle(double, double, Color, radius)");
    }

    public Circle(double x, double y, Color color) {
        this(x, y, color, DEFAULT_RADIUS);
        System.out.println("Circle(double, double, Color)");
    }

    public Circle(double x, double y) {
        super(x, y);
        this.radius = DEFAULT_RADIUS;
        System.out.println("Circle(double, double)");
    }

    public Circle() {
        super();
        radius = DEFAULT_RADIUS;
        System.out.println("Circle()");
    }

    @Override
    public void zoom(double magnification) {
        radius *= magnification;
        System.out.println("Circle: zoom()");
    }

    @Override
    public String toString() {
        return "Circle with " + super.toString()
                + " with radius: " + radius;
    }

    public double setRadius(double radius) {
        double oldRadius = this.radius;
        this.radius = radius;
        return oldRadius;
    }
}









