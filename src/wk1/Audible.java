package wk1;

public interface Audible {
    void mumble();
    void speak();
    void yell();
    void squeal();
}
