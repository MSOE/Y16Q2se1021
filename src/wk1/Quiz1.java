package wk1;

import java.util.ArrayList;
import java.util.List;

public class Quiz1 {
    public static void main(String[] args) {
        List<String> stuff = new ArrayList<>();
        stuff.add("The");
        stuff.add("quiz");
        stuff.add("was");
        stuff.add("easy");
        stuff.add("to");
        stuff.add("do");
        System.out.println(findShortest(stuff));
    }

    private static String findShortest(List<String> words) {
        String shortest = "";
        if(words!=null && !words.isEmpty()) {
            shortest = words.get(0);
            for(int i=1; i<words.size(); ++i) {
                if(shortest.length()>words.get(i).length()) {
                    shortest = words.get(i);
                }
            }
        }
        return shortest;
    }
}
