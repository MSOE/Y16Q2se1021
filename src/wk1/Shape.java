package wk1;

import java.awt.*;

public abstract class Shape {
    private Color color;
    private double x;
    private double y;

    public Color getColor() {
        return color;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    protected void setColor(Color color) {
        this.color = color;
    }

    protected void setX(double x) {
        this.x = x;
    }

    protected void setY(double y) {
        this.y = y;
    }

    public Shape() {
        this(0.0, 0.0);
        System.out.println("Shape()");
    }

    public Shape(double x, double y) {
        this(x, y, Color.BLACK);
        System.out.println("Shape(double, double)");
    }

    public Shape(double x, double y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
        System.out.println("Shape(double, double, Color)");
    }

    @Override
    public String toString() {
        return "Color: (" + color.getRed() + ", "
            + color.getGreen() + ", "
            + color.getBlue() + ") at (" + x + ", " + y + ")";
    }

    public void move(double deltaX, double deltaY) {
        x += deltaX;
        y += deltaY;
        System.out.println("Shape: move");
    }

    public abstract void zoom(double magnification);
}










