package wk1;

public class Rectangle extends Shape {
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        super();
        this.width = width;
        this.height = height;
    }

    @Override
    public void zoom(double magnification) {
        width *= magnification;
        height *= magnification;
        System.out.println("Rectangle: zoom()");
    }
}
