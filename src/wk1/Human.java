package wk1;

public class Human implements Audible {
    @Override
    public void mumble() {
        System.out.println("....mm...");
    }

    @Override
    public void speak() {
        System.out.println("hello world");
    }

    @Override
    public void yell() {
        System.out.println("HELLO WORLD");
    }

    @Override
    public void squeal() {
        System.out.println("eeeeeek");
    }
}
