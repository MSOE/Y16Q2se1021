package wk1;

public class Nothing {
    @Override
    public String toString() {
        return "Nothing object that does nothing and is worth nothing." +
                "Basically it is nothing; hence, the name Nothing.";
    }
}
