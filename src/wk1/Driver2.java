package wk1;

import javax.swing.*;

public class Driver2 {
    public static void main(String[] args) {
        int i = 0;
        System.out.println(i);    // 0
        System.out.println(i++);  // 0
        System.out.println(i);    // 1
        System.out.println(++i);  // 2
        System.out.println(i);    // 2
    }

    public static void main2(String[] args) {
        System.out.println((int)'a');
        String phrase = JOptionPane.showInputDialog(null, "Enter a phrase");
        JOptionPane.showMessageDialog(null, rot13phrase(phrase));
        JOptionPane.showMessageDialog(null, rot13phrase(rot13phrase(phrase)));
    }

    private static String rot13phrase(String phrase) {
        StringBuffer result = new StringBuffer();
        for(int i=0; i<phrase.length(); i++) {
            result.append(rot13(phrase.charAt(i)));
        }
        return result.toString();
    }

    private static char rot13(char c) {
        if(c>='a' && c<='z') {
            int position = c - 'a';
            position = (position + 13)%26;
            c = (char)(position + 'a');
        }
        if(c>='A' && c<='Z') {
            int position = c - 'A';
            position = (position + 13)%26;
            c = (char)(position + 'A');
        }
        return c;
    }

}
