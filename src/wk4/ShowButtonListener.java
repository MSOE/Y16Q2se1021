package wk4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowButtonListener implements ActionListener {
    private JLabel label;

    public ShowButtonListener(JLabel label) {
        this.label = label;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        label.setText(label.getText() + "Pony");
    }
}
