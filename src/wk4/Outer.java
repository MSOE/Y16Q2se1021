package wk4;

public class Outer {

    private int outerAttr = 0;
    private Inner in;

    private class Inner {
        private int innerAttr = 0;

        private void doStuff() {
            ++innerAttr;
            --outerAttr;
        }
    }

    public static void main(String[] args) {
        Outer out = new Outer();
        out.displayStuff();
    }

    public Outer() {
        in = new Inner();
    }

    public void displayStuff() {
        System.out.println("Inner: " + in.innerAttr + " Outer: " + outerAttr);
        in.doStuff();
        System.out.println("Inner: " + in.innerAttr + " Outer: " + outerAttr);
    }
}
