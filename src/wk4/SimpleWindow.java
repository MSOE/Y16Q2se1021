package wk4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SimpleWindow extends JFrame {

    private final static int WIDTH = 450;
    private final static int HEIGHT = 300;
    private JButton btnShow;
    private JButton btnHide;
    private JLabel ponyLabel;

    public static void main(String[] args) {
        JFrame gui = new SimpleWindow();
        gui.setVisible(true);
    }

    public SimpleWindow() {
        setTitle("Dog and Pony Show");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        add(new JLabel("Dog"));
        btnShow = new JButton("Show");
        btnShow.setActionCommand("good");
        ponyLabel = new JLabel("");
        btnShow.addActionListener(new ShowButtonListener());
        add(btnShow);
        btnHide = new JButton("Hide");
        btnHide.addActionListener(e -> handleHideButton());
        btnHide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("This is Friday");
                JOptionPane.showMessageDialog(SimpleWindow.this, "It's still Friday");
            }
        });
        btnHide.addMouseListener(new DeviousHider());
        btnShow.addMouseListener(new DeviousHider());
        add(btnHide);
        add(ponyLabel);
    }

    private void handleHideButton() {
        ponyLabel.setText("");
    }

    public JButton getShowButton() {
        return btnShow;
    }

    private class DeviousHider implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {
            setVisible(e, false);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            setVisible(e, true);
        }

        private void setVisible(MouseEvent e, boolean visible) {
            if(e.getSource() instanceof JButton) {
                JButton btn = (JButton)e.getSource();
                btn.setVisible(visible);
            }
        }
    }

    private class ShowButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            ponyLabel.setText(ponyLabel.getText() + "Pony");
        }
    }
}
