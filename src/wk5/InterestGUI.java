package wk5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Scanner;

public class InterestGUI extends JFrame {
    private JLabel labelTotal;
    private JTextField interestRateField;
    private double balance;
    private JTextArea balancesArea;
    private JButton addInterestButton;

    private static final double INITIAL_BALANCE = 1000.0;
    private final static int WIDTH = 450;
    private final static int HEIGHT = 300;

    public InterestGUI() {
        super("Interest Calculator");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        balance = INITIAL_BALANCE;
        createComponents();
    }

    private void createComponents() {
        addInterestButton = new JButton("Add Interest");
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double interestRate = 0.03;
                Scanner parser = new Scanner(interestRateField.getText());
                if(parser.hasNextDouble()) {
                    interestRate = parser.nextDouble()/100;
                }
                balance *= (1 + interestRate);
                balancesArea.append("\n" + ((int)(100*balance))/100.0);
            }
        };
        addInterestButton.addActionListener(listener);
        addInterestButton.addMouseMotionListener(new ChromaticAnnoyance());
        interestRateField = new JTextField("3.0", 10);
        interestRateField.setHorizontalAlignment(JTextField.RIGHT);
        interestRateField.addActionListener(listener);
        final int ROWS = 10;
        final int COLUMNS = 20;
        balancesArea = new JTextArea("Balance\n=======", ROWS, COLUMNS);
        balancesArea.setEditable(false);
        add(new JLabel("Interest rate: "));
        add(interestRateField);
        add(addInterestButton);
        add(new JScrollPane(balancesArea));
    }

    public static void main(String[] args) {
        JFrame gui = new InterestGUI();
        gui.setVisible(true);
    }

    private class ChromaticAnnoyance implements MouseMotionListener {
        @Override
        public void mouseDragged(MouseEvent e) {
            // Nothing to do
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();
            Color color = new Color(2*x, 10*y, x+5*y);
            addInterestButton.setBackground(color);
            double grayLevel = color.getRed()*0.299 + color.getGreen()*0.587 + color.getBlue()*0.114;
            if(grayLevel<128) {
                addInterestButton.setForeground(Color.WHITE);
            } else {
                addInterestButton.setForeground(Color.BLACK);
            }
        }
    }
}




















