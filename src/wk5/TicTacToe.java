package wk5;

/**************************************************************
 * TicTacToe.java
 * Dean & Dean
 * <p>
 * This program implements a simplified version of tic-tac-toe.
 * As a user clicks blank buttons, the buttons' labels change
 * to X and O in alternating sequence.
 **************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TicTacToe extends JFrame {
    private static final int WIDTH = 200;
    private static final int HEIGHT = 220;
    private boolean xTurn = true; // Is it X�s turn?

    //***********************************************************

    public TicTacToe() {
        setTitle("Tic-Tac-Toe");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        createContents();
        setVisible(true);
    } // end TicTacToe constructor

    //***********************************************************

    // Create components and add to window.

    private void createContents() {
        JButton button; // re-instantiate this button to fill entire board
        Listener listener = new Listener();

        setLayout(new GridLayout(3, 3));
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                button = new JButton();
                button.addActionListener(listener);
                add(button);
            } // end for j
        } // end for i
    } // end createContents

    //***********************************************************

    // If user clicks a button, change its label to "X" or "O".

    private class Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JButton btn = (JButton) e.getSource();
            if (btn.getText().isEmpty()) {
                btn.setText(xTurn ? "X" : "O");
                xTurn = !xTurn;
            }
        } // end actionPerformed
    } // end class Listener

    //***********************************************************

    public static void main(String[] args) {
        new TicTacToe();
    }
} // end class TicTacToe
