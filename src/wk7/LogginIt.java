package wk7;

import javax.swing.*;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogginIt {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("logger");
        try {
            logger.addHandler(new FileHandler("exceptions.log"));
            Scanner in = new Scanner(System.in);
            try {
                int number = in.nextInt();
                System.out.println("You entered: " + number);
            } catch(InputMismatchException e) {
                logger.log(Level.WARNING, "User entered non-integer value");
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Could not open log file", "IOException", JOptionPane.ERROR_MESSAGE);
        }
    }
}
